const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

function cb(a, b) {
    return a + b * b * b
}

function reduce(elements, cb, startingValue) {
    let startIndex = 0
    if (!startingValue) {
        startingValue = elements[0];
        startIndex = 1;
    }

    for (let index = startIndex; index < elements.length; index++) {
        startingValue = cb(startingValue, elements[index], index, elements)
    }
    return startingValue
}

console.log(reduce(items, cb))

module.exports = reduce