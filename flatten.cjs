const nestedArray = [1, [2], [[3, 7, [8, 9]]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements, depth = 1) {
    let flattenedArray = []
    for (let index = 0; index < elements.length; index++) {
        if (elements[index] instanceof Array && depth > 0) {
            flattenedArray.push(...flatten(elements[index], depth - 1))
        } else if (elements[index] !== undefined && elements[index] !== null) {
            flattenedArray.push(elements[index])
        }
    }
    return flattenedArray
}

let arr = flatten(nestedArray, 2)
console.log(arr)
module.exports = flatten