const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

function cb(number, index, elements) {
    return number * number;
}

function map(elements, cb) {
    let result = []
    if (!Array.isArray(elements) || typeof cb !== 'function') {
        return []
    }
    for (let index = 0; index < elements.length; index++) {
        if (elements[index]) {
            result.push(cb(elements[index], index, elements));
        }
    }
    return result
}

console.log(map(items, cb))

module.exports = map