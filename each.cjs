const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

function cb(number, index) {
    console.log(number)
}

function each(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index)
    }
}

each(items, cb)

module.exports = each;