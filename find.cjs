const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

function cb(number) {
    if (number === 3) {
        return true
    }
}

function find(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        let isthat = cb(elements[index])
        if (isthat) {
            return elements[index]
        }
    }
}

console.log(find(items, cb))

module.exports = find