const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

function cb(number) {
    return number > 3;
}

function filter(elements, cb) {
    let filteredelements = []
    for (let index = 0; index < elements.length; index++) {
        let isthat_true = cb(elements[index], index, elements)
        if (isthat_true === true) {
            filteredelements.push(elements[index])
        }
    }
    return filteredelements
}

console.log(filter(items, cb))

module.exports = filter